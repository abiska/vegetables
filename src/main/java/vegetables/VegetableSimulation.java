/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package vegetables;

/**
 *
 * @author iuabd
 */

import java.util.ArrayList;

public class VegetableSimulation {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //instance(s)
        Vegetable beet1 = new Beet("red", 2);
        Vegetable beet2 = new Beet("green", 1);
        
        Carrot carrot1 = new Carrot("orange", 1.5);
        Carrot carrot2 = new Carrot("yellow", 1);
        
        ArrayList <Vegetable> veggies= new ArrayList<>();
        veggies.add(beet1);
        veggies.add(beet2);
        veggies.add(carrot1);
        veggies.add(carrot2);
        
        for(int i=0; i<4; i++){
            System.out.println("The color of the Vegetable #" + (i+1) + " is " + veggies.get(i).getColor() + 
                    ", the size of Vegetable #" + (i+1) + " is " + veggies.get(i).getSize() + 
                    " which means it is Ripe: " + veggies.get(i).isRipe());
        }
    }
    
}
