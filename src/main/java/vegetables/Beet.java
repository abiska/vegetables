/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vegetables;

/**
 *
 * @author iuabd
 */
public class Beet extends Vegetable {
    
    //constructor(s)
    public Beet(String color, double size) {
        super(color, size);
    }
    
    //getters
    @Override    
    public double getSize() {return super.size;}
    public String getColor() {return super.color;}
    
    //method(s)
    @Override
    public boolean isRipe() {
        if(super.size == 2 && super.color == "red"){
            return true;
        } else {
            return false;
        }
    }

    
}
